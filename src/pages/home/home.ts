import {Component} from '@angular/core';

import {LoadingController, NavController, Platform} from 'ionic-angular';

import {InAppBrowser} from 'ionic-native';
import {StatusBar} from '@ionic-native/status-bar'
import {SplashScreen} from "@ionic-native/splash-screen";
import {Geolocation} from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private browser: InAppBrowser;
  public buttonTitle: string = 'Loading..';
  public buttonVisible: boolean = false;
  public spinnerVisible: boolean = false;
  private splash: SplashScreen;

  public loading: string = 'Only one moment...';

  private autoload: boolean = true;

  constructor(public platform: Platform, public navCtrl: NavController, private statusBar: StatusBar,
              splashScreen: SplashScreen, public loadingCtrl: LoadingController,
              public geolocation: Geolocation) {

    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        console.log('back button pressed');
        this.browser.close();
        navigator['app'].exitApp();
      });


      document.addEventListener("resume", function() {
        console.log("resumed app");
        this.autoload=false;
      }, false);

      console.log("Autoload enabled: "+this.autoload);
      if (this.autoload) {
        this.launch();
      } else {
        this.buttonVisible = true;
        splashScreen.hide();
      }
      this.splash = splashScreen;

    });



  }

  launch() {

    this.geolocation.getCurrentPosition().then((position) => {

      console.log(position.coords.latitude, position.coords.longitude);

    }, (err) => {
      console.log(err);
    });

    let loader = this.loadingCtrl.create({
      content: this.loading
    });
    loader.present();

    this.spinnerVisible = true;
    this.buttonVisible = false;
    this.buttonTitle = 'Loading...';
    let url: string = 'https://www.livearound.it/churcharound';
    this.browser = new InAppBrowser(url, '_blank', 'EnableViewPortScale=yes,location=no,hidden=yes,zoom=no,toolbar=no');
    // browser.on('loadstart').subscribe((ev: InAppBrowserEvent) => {
    //  console.log('dajeeeeee');
    // });
    this.browser.on('loadstop').subscribe(() => {
      console.log("InAppBrowser Loadstop Event OK: ");
      this.buttonTitle = 'Back to map';
      loader.dismissAll();
      this.browser.show();
      this.buttonVisible = true;
      this.spinnerVisible = false;
      this.autoload = false;
      this.splash.hide();
    }, err => {
      console.log("InAppBrowser Loadstop Event Error: " + err);
      loader.dismiss();
      this.buttonTitle = 'ERROR';
      this.browser.close();
      this.buttonVisible = true;
      this.spinnerVisible = false;
    });

    this.browser.on('exit').subscribe(() => {
      console.log("InAppBrowser exit Event OK: ");
      this.browser.close();
      loader.dismissAll();
    }, err => {
      console.log("InAppBrowser Loadstop Event Error: " + err);
      this.browser.close();
      loader.dismissAll();
    });
  }
}
